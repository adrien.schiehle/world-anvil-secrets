import * as browserUpgrade from './module/waBrowserUpgrade.js';
import * as journalUpgrade from './module/journalEntryUpgrade.js';

/**
 * WA Secret are parse differently: Each <h3> in the parsed content is considerated as a distinct secret
 */
Hooks.on("WAParseArticle", (article, result) => {
  journalUpgrade.parseArticle( article, result );
});


/**
 * When updating an existing journal Entry, we need to retrieve old values for all existing secrets
 */
 Hooks.on("WAUpdateJournalEntry", (entry, content) => {
  journalUpgrade.updateCurrentJournalEntryFlags(entry, content.waFlags);
});

/**
 * Augment rendered Journal sheets to add secret sections management
 */
Hooks.on("renderJournalSheet", (app, html, data) => {

  // Only for journal entries linked to a WA article
  const journalEntry = app.object;
  if( ! journalEntry.getFlag("world-anvil", "articleId") ) return;

  if( !game.user.isGM ) {
    journalUpgrade.filterDisplayedJournalPages(html, journalEntry);
  }
});

/**
 * Augment rendered Journal sheets to add secret sections management
 */
 Hooks.on("renderJournalPageSheet", (app, html, data) => {

  const journalEntry = app.object.parent;
  if( ! journalEntry.getFlag("world-anvil", "articleId") ) return;

  const secretFlag = journalEntry.getFlag("world-anvil", "secrets") ?? {};
  const removedAnchors = journalUpgrade.renderSecretSections(html, secretFlag);
  journalUpgrade.filterDisplayedJournalPageDetail(html, journalEntry, removedAnchors);
  if( game.user.isGM ) {
    journalUpgrade.addToggleSecretsListeners(html, journalEntry);
  }
 });

/**
 * Augment rendered WorldAnvilBrowser sheet to modify secret button
 */
Hooks.on("renderWorldAnvilBrowser", (app, html, data) => {

	// Update html content by substituting has-secret buttons
  browserUpgrade.addSecretButtonsInBrower(html);

  // Add listener for this buttons
  html.find(".world-anvil-control.has-secret.reveal-secrets").click( event => browserUpgrade.revealSecrets(event, app) );
  html.find(".world-anvil-control.has-secret.hide-secrets").click( event => browserUpgrade.hideSecrets(event, app) );

});

//----------------------------------
// Also do the same thing one Actor sheet 
// in case world-module-actors is present
//-----------------------------------

/**
 * Augment rendered Journal sheets to add secret sections management
 */
 Hooks.on("renderActorSheet", (app, html, data) => {

  // Only for characters linked to a WA article
  const actor = app.object;
  if( ! actor.getFlag("world-anvil", "articleId") ) return;

  const secretFlag = actor.getFlag("world-anvil", "secrets") ?? {};
  journalUpgrade.renderSecretSections(html, secretFlag);
  if( game.user.isGM ) {
    journalUpgrade.addToggleSecretsListeners(html, actor);
  }
});

